export GITLAB_USER=michal.rusnak294
export GITLABURL=https://gitlab.com/michal.rusnak294/flux-k-3-s
export GITLAB_TOKEN=xxx

fluxctl install \
 --git-user=${GITLABUSER} \
 --git-email=${GITLABUSER}@gmail.com \
 --git-url=git@gitlab.com:${GITLABURL} \
 --git-branch=main \
 --git-path=mycv-deployment.yaml,mycv-service.yaml \
 --namespace=flux | sudo k3s kubectl apply -f -

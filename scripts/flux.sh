#!/bin/bash
curl -s https://fluxcd.io/install.sh | sudo bash

export GITLAB_USER=michal.rusnak294
export GITLABURL=https://gitlab.com/michal.rusnak294/flux-k-3-s
export GITLAB_TOKEN=xxx

flux check --pre

flux install

flux bootstrap gitlab \
  --owner=$GITLAB_USER \
  --repository=flux-k-3-s \
  --branch=main \
  --path=mycv-deployment.yaml,mycv-service.yaml \
  --personal \
  --token-auth



wget https://gitlab.com/michal.rusnak294/flux-k-3-s/-/raw/main/flux-sync.yaml
sudo k3s kubectl apply -f flux-sync.yaml

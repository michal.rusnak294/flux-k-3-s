#!/bin/bash
wget https://github.com/fluxcd/flux/releases/download/1.25.4/fluxctl_linux_amd64
sudo mv fluxctl_linux_amd64 fluxctl
sudo chmod +x fluxctl
sudo mv fluxctl /usr/local/bin/

sudo k3s kubectl create ns flux

export GITLAB_USER=michal.rusnak294
export GITLABURL=https://gitlab.com/michal.rusnak294/flux-k-3-s
export GITLAB_TOKEN=xxx

sudo fluxctl install \
 --git-user=${GITLABUSER} \
 --git-email=${GITLABUSER}@gmail.com \
 --git-url=git@gitlab.com:${GITLABURL} \
 --git-branch=main \
 --git-path=mycv-deployment.yaml,mycv-service.yaml \
 --namespace=flux | sudo k3s kubectl apply -f -

sudo k3s kubectl rollout status -n flux deployment/flux

sudo fluxctl identity --k8s-fwd-ns flux

export FLUX_FORWARD_NAMESPACE=flux
sudo fluxctl sync --k8s-fwd-ns flux

wget https://gitlab.com/michal.rusnak294/flux-k-3-s/-/raw/main/flux-sync.yaml
sudo k3s kubectl apply -f flux-sync.yaml

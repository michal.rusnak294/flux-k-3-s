#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`
##

# Install packages
apt -y install git
##

# Create User
useradd -s /bin/bash -c "Admin" -m michal
echo "Passw0rd" | passwd --stdin michal
echo "michal ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
##

# Deploy SSH keys
mkdir /home/michal/.ssh
cat <<EOF | tee -a /home/michal/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDY2iqmgwxok8oHGzNwQ6OuFITD/ecDjY//eQ1xmWAsB30n7ysC/wkIozuwKXaj2ubKI47tYPCev4ObulrciW3P90yBqRY2Ub+qDhfRFi83wFm8u98igavvb7KbHv8ZSK5mGFa3ykpMjqG2T2zF5fGMKVwE+1o7p9tkCi7hhCzcDHir5hCR0wblqjDqWlkP74g+kislVPK0PAnzYu1Z7QdAxKIGTbPdTEzganePOheX5SLzxrHLZExllN75CJZ+Bz/GZU2xN4UeobfViAq+zWrSs51kffWjQmR+KoyP/TcXm6LJDQHzSzX46XhTBIdGB1wLsYLTgAhpYqj/DJLsTB8d Intel@PC
EOF
# Set proper permissions
chown -R michal /home/michal/.ssh
chmod 700 /home/michal/.ssh
chmod 600 /home/michal/.ssh/authorized_keys
##

## Init K3S
curl -sfL https://get.k3s.io | sh -
systemctl stop k3s
##

## Join to the Cluster
sleep 3m
masterip=10.0.1.69
wget http://${masterip}:8080/token 
mastertoken=`cat token`
k3s agent --server https://${masterip}:6443 --token ${mastertoken}
##

